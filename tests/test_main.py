from fastapi.testclient import TestClient

from main import app


def test_root():
    with TestClient(app) as client:
        response = client.get("/")
        assert response.status_code == 200
        assert response.json() == {"message": "Hello World"}


def test_say_hello():
    with TestClient(app) as client:
        response = client.get("/hello/foo")
        assert response.status_code == 200
        assert response.json() == {"message": "Hello foo"}

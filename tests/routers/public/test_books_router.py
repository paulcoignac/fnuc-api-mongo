from fastapi.testclient import TestClient

from main import app


# Creer un book, un topic et un keyword pour la suite des tests et utiliser leurs id

def test_get_books():
    with TestClient(app) as client:
        response = client.get("books")
        assert response.status_code == 200


def test_get_book():
    with TestClient(app) as client:
        response = client.get("books/649165d81f2a633d445d2e4c")
        assert response.status_code == 200


def test_get_book_not_exist():
    with TestClient(app) as client:
        response = client.get("books/1")
        assert response.status_code == 422


def test_get_keywords():
    with TestClient(app) as client:
        response = client.get("books/6491680ddc3a98a4622dd2cb/keywords")
        assert response.status_code == 200


def test_get_book_keywords_wrong_book():
    with TestClient(app) as client:
        response = client.get("books/test/keywords")
        assert response.status_code == 422

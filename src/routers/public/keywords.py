from typing import List

from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue, mongo_to_vue_list
from services import keywords_service
from vues.book import BookVue
from vues.keyword import KeywordVue

router = APIRouter(
    prefix="/keywords",
    tags=["keywords"],
)


@router.get("/",response_model=List[KeywordVue],description="Get all keywords")
async def get_keywords():
    keywords = await keywords_service.get_keywords()
    return mongo_to_vue_list(keywords,KeywordVue)


@router.get("/{id}",description="Get a keyword",response_model=KeywordVue)
async def get_keyword(id: PydanticObjectId):
    keyword = await keywords_service.get_keyword(id)
    return mongo_to_vue(keyword.dict(),KeywordVue)


@router.get("/{id}/books",description="Get all books of a keyword",response_model=List[BookVue])
async def get_keyword_books(id: PydanticObjectId) -> List[BookVue]:
    books = await keywords_service.get_keyword_books(id)
    return mongo_to_vue_list(books,BookVue)

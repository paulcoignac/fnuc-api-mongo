from typing import List

from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue_list, mongo_to_vue
from services import topics_service
from vues.book import BookVue
from vues.topic import TopicVue

router = APIRouter(
    prefix="/topics",
    tags=["topics"],
)


@router.get("/", description="Get all topics", response_model=List[TopicVue])
async def get_topics():
    topics = await topics_service.get_topics()
    return mongo_to_vue_list(topics, TopicVue)


@router.get("/{id}", description="Get a topic", response_model=TopicVue)
async def get_topic(id: PydanticObjectId):
    topic = await topics_service.get_topic(id)
    return mongo_to_vue(topic.dict(), TopicVue)


@router.get("/{id}/books", description="Get all books of a topic", response_model=List[BookVue])
async def get_topic_books(id: PydanticObjectId):
    books = await topics_service.get_topic_books(id)
    return mongo_to_vue_list(books, BookVue)
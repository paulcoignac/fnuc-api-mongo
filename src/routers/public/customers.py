from typing import Annotated

from fastapi import APIRouter, Depends

from internal.auth import JWTBearer
from models.customers import Customer
from services import customers_service

router = APIRouter(
    prefix="/customers",
    tags=["customers"],
)


@router.get("/")
async def get_user(token: Annotated[str, Depends(JWTBearer())]):
    return customers_service.get_user(token)

@router.delete("/")
async def delete_user(token: Annotated[str, Depends(JWTBearer())]):
    return customers_service.delete_user(token)

@router.put("/")
async def update_user(token: Annotated[str, Depends(JWTBearer())], customer: Customer):
    return customers_service.modify_user(token,customer)














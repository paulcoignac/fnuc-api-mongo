from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue
from models.keywords import Keyword
from services import keywords_service
from vues.keyword import KeywordVue

router = APIRouter(
    prefix="/keywords",
    tags=["keywords"],
)


@router.post("/")
async def post_keyword(keyword:KeywordVue):
    return keywords_service.create_keyword(mongo_to_vue(keyword.dict(),Keyword))

@router.delete("/{id}")
async def delete_keyword(id:PydanticObjectId):
    return keywords_service.delete_keyword(id)


@router.put("/{id}",response_model=KeywordVue,description="Update a keyword")
async def put_keyword(id:PydanticObjectId,keyword:KeywordVue):
    keyword = await keywords_service.update_keyword(id, mongo_to_vue(keyword.dict(), Keyword))
    return  mongo_to_vue(keyword.dict(), KeywordVue)

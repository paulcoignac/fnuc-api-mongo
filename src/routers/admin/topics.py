from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue
from models.topics import Topic
from services import topics_service
from vues.topic import TopicVue

router = APIRouter(
    prefix="/topics",
    tags=["topics"],
)


@router.post("/")
async def post_topic(topic: TopicVue) -> TopicVue:
    topic = await topics_service.create_topic(mongo_to_vue(topic.dict(), Topic))
    return mongo_to_vue(topic.dict(), TopicVue)

@router.delete("/{id}", description="Delete a topic", response_model=TopicVue)
async def delete_topic(id: PydanticObjectId):
    await topics_service.delete_topic(id)

@router.put("/{id}", response_model=TopicVue, description="Update a topic")
async def put_topic(id: PydanticObjectId, topic: TopicVue) -> TopicVue:
    topic = await topics_service.update_topic(id, mongo_to_vue(topic.dict(), Topic))
    return mongo_to_vue(topic.dict(), TopicVue)


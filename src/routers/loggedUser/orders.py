from beanie import PydanticObjectId
from fastapi import APIRouter, Depends

from adapters.mongo_vue_adapter import mongo_to_vue
from internal.auth import JWTBearer
from models.orders import Order
from services import orders_service
from vues.order import OrderVue

router = APIRouter(
    prefix="/orders",
    tags=["orders"],
)
@router.get("/{id}", response_model=OrderVue, description="Get an order")
async def get_order(id: PydanticObjectId) -> OrderVue:
    order = await orders_service.get_order(id)
    return mongo_to_vue(order.dict(), OrderVue)


@router.post("/", response_model=OrderVue, description="Create an order")
async def post_order(order: OrderVue) -> OrderVue:
    order = await orders_service.create_order(mongo_to_vue(order.dict(), Order))
    return mongo_to_vue(order.dict(), OrderVue)

@router.put("/{id}", response_model=OrderVue, description="Update an order")
async def put_order(id: PydanticObjectId, order: OrderVue) -> OrderVue:
    order = await orders_service.update_order(id, mongo_to_vue(order.dict(), Order))
    return mongo_to_vue(order.dict(), OrderVue)

@router.delete("/{id}", description="Delete an order")
async def delete_order(id: PydanticObjectId):
    await orders_service.delete_order(id)
from beanie import PydanticObjectId
from fastapi import APIRouter

router = APIRouter(
    prefix="/inventories",
    tags=["inventories"],
)
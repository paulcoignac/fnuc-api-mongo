import os

from beanie import init_beanie
from dotenv import load_dotenv
from motor.motor_asyncio import AsyncIOMotorClient

from models.books import Book
from models.customers import Customer
from models.keywords import Keyword
from models.orders import Order
from models.topics import Topic

load_dotenv()
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DOCKER = os.getenv('DOCKER')
CI = os.getenv('CI')

connection_uri = f"mongodb://{DB_USER}:{DB_PASSWORD}@localhost:{DB_PORT}/{DB_NAME}"
if DOCKER:
    connection_uri = f"mongodb://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
if CI:
    connection_uri = f"mongodb://mongo:27017/{DB_NAME}"


async def init_db():
    db = AsyncIOMotorClient(
        host=connection_uri,
    )[DB_NAME]
    await init_beanie(database=db, document_models=[Book, Customer, Topic, Keyword, Order])


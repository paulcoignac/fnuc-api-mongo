import os

from dotenv import load_dotenv

load_dotenv()

url = os.getenv('AUTH_HOST')

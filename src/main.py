from fastapi import FastAPI, Depends

from config.db import init_db
from internal.auth import is_admin, JWTBearer
from routers.admin import books as admin_books, orders as admin_orders, customers as admin_customers, \
    keywords as admin_keywords, \
    topics as admin_topics
from routers.loggedUser import inventories as loggedUser_inventories, orders as loggedUser_orders
from routers.public import books, keywords, customers, topics, auth

app = FastAPI(
    prefix="/api",
)

app.include_router(books.router)
app.include_router(admin_books.router, dependencies=[Depends(is_admin)])
app.include_router(loggedUser_inventories.router, dependencies=[Depends(JWTBearer())])
app.include_router(loggedUser_orders.router, dependencies=[Depends(JWTBearer())])
app.include_router(loggedUser_orders.router, dependencies=[Depends(JWTBearer())])
app.include_router(admin_orders.router, dependencies=[Depends(is_admin)])
app.include_router(keywords.router)
app.include_router(keywords.router)
app.include_router(customers.router)
app.include_router(admin_customers.router, dependencies=[Depends(is_admin)])
app.include_router(admin_keywords.router, dependencies=[Depends(is_admin)])
app.include_router(admin_topics.router, dependencies=[Depends(is_admin)])
app.include_router(topics.router)
app.include_router(auth.router)


@app.on_event("startup")
async def start_db():
    await init_db()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}

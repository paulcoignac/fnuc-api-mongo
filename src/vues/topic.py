from beanie import PydanticObjectId
from pydantic import BaseModel


class TopicVue(BaseModel):
    id: PydanticObjectId
    label: str
    image: str

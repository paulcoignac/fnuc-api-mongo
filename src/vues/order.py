from datetime import datetime

from beanie import PydanticObjectId
from pydantic import BaseModel


class OrderVue(BaseModel):
    id: PydanticObjectId
    id_book: PydanticObjectId
    id_customer: PydanticObjectId
    quantity: float  # TODO a limiter à 10 après la virgule
    date: datetime = datetime.now()

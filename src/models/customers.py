from beanie import Document, init_beanie
from typing import Optional


class Customer(Document):
    firstName: str
    lastName: str
    email: str
    password: str
    cumulSales: Optional[float]
    role: str

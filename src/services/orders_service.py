from beanie import PydanticObjectId

from models.orders import Order
from services.books_service import decrease_book_stock, increase_book_stock


async def get_orders() -> list[Order]:
    """
    Get all orders
    :return: all orders
    """
    return await Order.find_all().to_list()

async def get_order(id: PydanticObjectId) -> Order:
    """
    Get an order
    :param id: id of the order
    :return: the order
    """
    return await Order.get(id)

async def create_order(order: Order) -> Order:
    """
    Create an order
    :param order: order to create
    :return:   the created order
    """
    await order.create()
    await decrease_book_stock(order.id_book,order.quantity)
    return order

async def update_order(id: PydanticObjectId, order: Order):
    """
    Update an order
    :param id: id of the order
    :param order: values to update
    :return:    updated order
    """
    order_update = {k: v for k, v in order.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in order_update.items()
    }}
    order = await Order.get(id)
    await order.update(update_query)
    return order

async def delete_order(id: PydanticObjectId):
    """
    Delete an order
    :param id: id of the order
    :return:
    """
    order = await Order.get(id)
    await increase_book_stock(order.id_book,order.quantity)
    await order.delete()







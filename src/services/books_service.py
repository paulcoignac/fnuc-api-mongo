from beanie import PydanticObjectId

from models.keywords import Keyword
from models.books import Book, UpdateBook
from fastapi import HTTPException

from models.orders import Order
from models.topics import Topic


async def create_book(book: Book) -> Book:
    """
    Create a book
    :param book: book to create
    :return:  the created book
    """
    return await book.create()


async def get_books() -> list[Book]:
    """
    Get all books
    :return: all books
    """
    return await Book.find_all().to_list()


async def get_book(id: PydanticObjectId) -> Book:
    """
    Get a book
    :param id: id of the book
    :return: the book
    """
    return await Book.get(id)


async def get_book_keywords(id: PydanticObjectId) -> list[Keyword]:
    """
    Get all keywords for a book
    :param id: id of the book
    :return: the keywords
    """
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    if not book.keywords:
        return []
    return book.keywords;


async def get_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId) -> Keyword:
    """
    Get a keyword for a book
    :param id: id of the book
    :param keyword_id: keyword id
    :return: the keyword
    """
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword not in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword not found")
    return keyword


async def get_book_topics(id: PydanticObjectId) -> list[Topic]:
    """
    Get all topics for a book
    :param id: id of the book
    :return: the topics
    """
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    if not book.topics:
        return []
    return book.topics;


async def update_book(id: PydanticObjectId, book_update: UpdateBook):
    """
    Update a book
    :param id: id of the book
    :param book_update: values to update
    :return: the updated book
    """
    book_update = {k: v for k, v in book_update.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in book_update.items()
    }}
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    await book.update(update_query)
    return book


async def delete_book(id: PydanticObjectId):
    """
    Delete a book
    :param id: id of the book
    :return:
    """
    book = await Book.get(id)
    await book.delete()


async def get_book_orders(id: PydanticObjectId) -> list[Order]:
    """
    Get all orders for a book
    :param id: id of the book
    :return: book orders
    """
    return await Order.find_all(Order.book == id).to_list()


async def get_book_order(id: PydanticObjectId, order_id: PydanticObjectId) -> Order:
    """
    Get a specific order for a book
    :param id: id of the book
    :param order_id: id of the order
    :return: the order
    """
    return await Order.find_one(Order.book == id, Order.id == order_id)



async def remove_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId):
    """
    Remove a keyword from a book
    :param id: id of the book
    :param keyword_id: id of the keyword
    :return:
    """
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword not in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword not found")
    await book.update({"$pull": {"keywords": keyword}})

async def add_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId):
    """
    Add a keyword to a book
    :param id:
    :param keyword_id:
    :return:
    """
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword already exists")
    await book.update({"$push": {"keywords": keyword}})

async def remove_book_topic(id: PydanticObjectId, topic_id: PydanticObjectId):
    """
    Remove a topic from a book
    :param id: id of the book
    :param topic_id: id of the topic
    :return:
    """
    book = await Book.get(id)
    topic = await Topic.get(topic_id)
    if topic not in book.topics:
        raise HTTPException(status_code=404, detail="Topic not found")
    await book.update({"$pull": {"topics": topic}})

async def add_book_topic(id: PydanticObjectId, topic_id: PydanticObjectId):
    """
    Add a topic to a book
    :param id: id of the book
    :param topic_id:  id of the topic
    :return:
    """
    book = await Book.get(id)
    topic = await Topic.get(topic_id)
    if topic in book.topics:
        raise HTTPException(status_code=404, detail="Topic already exists")
    await book.update({"$push": {"topics": topic}})


async def increase_book_stock(id: PydanticObjectId, quantity: int):
    """
    Increase the stock of a book
    :param id: id of the book
    :param quantity: quantity to increase
    :return: the book
    """
    book = await Book.get(id)
    await book.update({"$inc": {"stock": quantity}})
    return book

async def decrease_book_stock(id: PydanticObjectId, quantity: int):
    """
    Decrease the stock of a book
    :param id: id of the book
    :param quantity: quantity to decrease
    :return: the book
    """
    book = await Book.get(id)
    await book.update({"$inc": {"stock": -quantity}})
    return book

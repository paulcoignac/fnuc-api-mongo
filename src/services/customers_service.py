from beanie import PydanticObjectId
from fastapi import HTTPException
import requests
from starlette.responses import JSONResponse

from internal.auth_requests import call_auth
from models.customers import Customer
from models.login import Login
from config import auth
from models.register import Register





def login(login: Login) :
    """
    Login a user
    :param login: login of the user
    :return:  the token of the user
    """
    [response, response_content] = call_auth(method='post', path='login', json=login.dict())
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content

def register(register:Register) :
    """
    Register a user
    :param register: register of the user
    :return:  the token of the user
    """
    [response, response_content] = call_auth(method='post', path='register',json=register.dict())
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content

def logout(token: str) :
    """
    Logout a user
    :param token: token of the user
    :return:
    """

    [response, response_content] = call_auth(method='post', path='logout', token_user=token)
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content

def get_user(token: str) :
    """
    Get a user
    :param token: token of the user
    :return:  the user
    """
    [response, response_content] = call_auth(method='get', path='user', token_user=token)
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content

def delete_user(token: str) :
    """
    Delete a user
    :param token: token of the user
    :return:
    """
    [response, response_content] = call_auth(method='delete', path='user', token_user=token)
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content


def modify_user(token: str, user: Customer) :
    """
    Modify a user
    :param token: token of the user
    :param user: user to modify
    :return:
    """
    [response, response_content] = call_auth(method='put', path='user', token_user=token, json=user.dict())
    if response.status_code != 200:
        return JSONResponse(status_code=response.status_code, content=response_content)
    return response_content


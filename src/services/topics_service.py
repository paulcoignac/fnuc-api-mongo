from typing import List

from beanie import PydanticObjectId
from fastapi import HTTPException

from models.books import Book
from models.topics import Topic


async def create_topic(topic: Topic) -> Topic:
    """
    Create a topic
    :param topic: topic to create
    :return:  the created topic
    """
    return await topic.create()


async def get_topics() -> List[Topic]:
    """
    Get all topics
    :return: all topics
    """
    return await Topic.find_all().to_list()


async def get_topic(id: PydanticObjectId) -> Topic:
    """
    Get a topic
    :param id: id of the topic
    :return: the topic
    """
    return await Topic.get(id)


async def get_topic_books(id: PydanticObjectId) -> List[Book]:
    """
    Get all books for a topic
    :param id: id of the topic
    :return: the books
    """
    topic = await Topic.get(id)
    if not topic:
        raise HTTPException(status_code=404, detail="Topic not found")
    if not topic.books:
        return []
    return topic.books

async def delete_topic(id: PydanticObjectId):
    """
    Delete a topic
    :param id: id of the topic
    :return:
    """
    topic = await Topic.get(id)
    await topic.delete()

async def update_topic(id: PydanticObjectId, topic: Topic):
    """
    Update a topic
    :param id: id of the topic
    :param topic: values to update
    :return: updated topic
    """
    topic_update = {k: v for k, v in topic.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in topic_update.items()
    }}
    topic = await Topic.get(id)
    await topic.update(update_query)
    return topic

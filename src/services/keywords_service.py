from beanie import PydanticObjectId
from fastapi import HTTPException

from models.books import Book
from models.keywords import Keyword


async def get_keywords() -> list[Keyword]:
    """
    Get all keywords
    :return: all keywords
    """
    return await Keyword.find_all().to_list()


async def get_keyword(id: PydanticObjectId) -> Keyword:
    """
    Get a keyword
    :param id: id of the keyword
    :return: the keyword
    """
    return await Keyword.get(id)


async def get_keyword_books(id: PydanticObjectId) -> list[Book]:
    """
    Get all books for a keyword
    :param id: id of the keyword
    :return: the books
    """
    return await Book.find({'keywords._id': id}).to_list()
async def create_keyword(keyword: Keyword) -> Keyword:
    """
    Create a keyword
    :param keyword: keyword to create
    :return: the created keyword
    """
    await keyword.create()
    return keyword

async def update_keyword(id: PydanticObjectId, keyword: Keyword):
    """
    Update a keyword
    :param id: id of the keyword
    :param keyword: values to update
    :return: updated keyword
    """
    keyword_update = {k: v for k, v in keyword.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in keyword_update.items()
    }}
    keyword = await Keyword.get(id)
    await keyword.update(update_query)
    return keyword

async def delete_keyword(id: PydanticObjectId):
    """
    Delete a keyword
    :param id: id of the keyword
    :return:
    """
    keyword = await Keyword.get(id)
    await keyword.delete()


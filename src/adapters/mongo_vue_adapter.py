from typing import TypeVar, Type

T = TypeVar('T')


def mongo_to_vue(mongo_data: dict, clazz: Type[T]) -> T:
    vue = {}
    for key, value in mongo_data.items():
        if key == '_id':
            key = 'id'
        vue[key] = value
    return clazz.parse_obj(vue)


def mongo_to_vue_list(mongo_data: list, clazz: Type[T]) -> list[T]:
    vue = []
    for item in mongo_data:
        vue.append(mongo_to_vue(item.dict(), clazz))
    return vue


def vue_to_mongo(vue_data: dict, clazz: Type[T]) -> T:
    mongo = {}
    for key, value in vue_data.items():
        if key == 'id':
            key = '_id'
        mongo[key] = value
    return clazz.parse_obj(mongo)


def vue_to_mongo_list(vue_data: list, clazz: Type[T]) -> list[T]:
    mongo = []
    for item in vue_data:
        mongo.append(vue_to_mongo(item.dict(), clazz))
    return mongo

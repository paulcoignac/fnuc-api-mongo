db = db.getSiblingDB('fnuc');

db.createUser(
    {
        user: 'api',
        pwd: 'api',
        roles: [{role: 'readWrite', db: 'fnuc'}]
    }
);

db.createCollection('Order');
db.createCollection('Book');
db.createCollection('Keyword');
db.createCollection('Topic');
// Exemples d'insertion dans la collection "Order"


// Exemples d'insertion dans la collection "Topic"
db.Topic.insertMany([
  {
    title: 'Fiction',
    image: 'https://example.com/fiction.jpg'
  },
  {
    title: 'Philosophy',
    image: 'https://example.com/philosophy.jpg'
  },
  // Ajoutez d'autres exemples d'insertion ici
]);

// Exemples d'insertion dans la collection "Keyword"
db.Keyword.insertMany([
  {
    label: 'philosophy'
  },
  {
    label: 'fiction'
  },
  // Ajoutez d'autres exemples d'insertion ici
])

console.log(db.Keyword.find({label: 'philosophy'}));
console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")

// Exemples d'insertion dans la collection "Book"
db.Book.insertMany([
  {
    title: 'Le Petit Prince',
    author: 'Antoine de Saint-Exupéry',
    resume: 'Un conte poétique et philosophique',
    price: 12.99,
    image: 'https://example.com/book-image.jpg',
    keywords: db.Keyword.find({label: 'philosophy'}),
    topics: db.Topic.find({title: 'Fiction'}),
    stock: {
      level: 10,
      security: 5
    }
  },
  {
    title: '1984',
    author: 'George Orwell',
    resume: 'A dystopian novel',
    price: 9.99,
    image: 'https://example.com/1984.jpg',
    keywords: db.Keyword.find({label: 'fiction'}),
    topics: db.Topic.find({title: 'Fiction'}),
    stock: {
      level: 5,
      security: 2
    }
  },
  // Ajoutez d'autres exemples d'insertion ici
]);


db.Order.insertMany([
  {
    article: db.Book.find({title: 'Le Petit Prince'}),
    customerEmail: 'john@example.com',
    quantity: 5.25,
    date: new Date()
  },
  {
    article: db.Book.find({title: '1984'}),
    customerEmail: 'jane@example.com',
    quantity: 3.75,
    date: new Date()
  },
  // Ajoutez d'autres exemples d'insertion ici
]);



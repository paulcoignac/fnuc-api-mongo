const MongoClient = require('mongodb').MongoClient;
const dotenv = require("dotenv")
dotenv.config()
url = "mongodb://localhost:27017/";
connection_uri = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@localhost:${process.env.DB_PORT}/${process.env.DB_NAME}`
populateDB();

function getKeywords(dbo, label) {
    return new Promise(async (resolve) => {
            const collection = dbo.collection("Keyword");
            const keywords = [];
            await collection.find().forEach((doc) => {
                keywords.push(doc);
            });
            resolve(keywords);
        }
    )
}

function getTopics(dbo, title) {
    return new Promise(async (resolve) => {
            const topics = [];

            await dbo.collection("Topic").find().forEach((doc) => {
                    topics.push(doc);
                }
            );
            resolve(topics);
        }
    )
}

function getBook(dbo, title) {
    return new Promise(async (resolve) => {
            const books = [];
            await dbo.collection("Book").find().forEach((doc) => {
                    books.push(doc);
                }
            );
            resolve(books);
        }
    )
}

async function populateDB() {
    const client = new MongoClient(
        connection_uri,
    );
    await client.connect();
    const dbo = client.db(process.env.DB_NAME);
    await dbo.collection("Keyword").insertMany([
        {
            label: 'philosophy'
        }
        ,
        {
            label: 'fiction'
        }
        ,
        {
            label: 'science'
        }
        ,
        {
            label: 'history'
        }]
    );
    await dbo.collection("Topic").insertMany([
        {
            label: 'Fiction',
            image: 'https://example.com/fiction.jpg'
        },
        {
            label: 'Philosophy',
            image: 'https://example.com/philosophy.jpg'
        }
        ,
        {
            label: 'Science',
            image: 'https://example.com/science.jpg'
        }])
    const keywords = await getKeywords(dbo, 'philosophy')
    const topics = await getTopics(dbo, 'Fiction')
    await dbo.collection("Book").insertMany([
        {
            title: 'Le Petit Prince',
            author: 'Antoine de Saint-Exupéry',
            resume: 'Un conte poétique et philosophique',
            price: 12.99,
            image: 'https://example.com/book-image.jpg',
            keywords: keywords,
            topics: topics,
            stock: {
                level: 10,
                security: 5
            },
        },
        {
            title: '1984',
            author: 'George Orwell',
            resume: 'A dystopian novel',
            price: 9.99,
            image: 'https://example.com/1984.jpg',
            keywords: keywords,
            topics: topics,
            stock: {
                level: 5,
                security: 2
            }
        }])
    const books = await getBook(dbo)
    await dbo.collection("Order").insertMany([
        {
            id_book: books[1]._id,
            id_customer: 'john@example.com',
            quantity: 5.25,
            date: new Date()
        },
        {
            id_book: books[0]._id,
            id_customer: 'jane@example.com',
            quantity: 3.75,
            date: new Date()
        },])

    client.close();
}


![coverage](https://gitlab.com/paulcoignac/fnuc-api-mongo/badges/master/coverage.svg)

# fnuc-api-mongo

## Windows

Installer [Docker Desktop](https://www.docker.com/products/docker-desktop)

Puis lancer la commande `docker compose up --build -d`

## Linux

Installer [Docker](https://docs.docker.com/engine/install/ubuntu/)

Installer [Docker Compose](https://docs.docker.com/compose/install/)

Puis lancer la commande `docker compose up --build -d`

## Urls

- localhost:8000: API
- localhost:8081: Mongo Express
- localhost:27017: MongoDB

## Exemple de fichier .env


#DB CONFIG

DB_HOST=db

DB_PORT=27017

DB_USER=api

DB_PASSWORD=api

DB_NAME=fnuc

MONGO_INITDB_ROOT_USERNAME: fnuc-admin

MONGO_INITDB_ROOT_PASSWORD: 123

#MONGO EXPRESS CONFIG

ME_CONFIG_MONGODB_ADMINUSERNAME: fnuc-admin

ME_CONFIG_MONGODB_ADMINPASSWORD: 123

ME_CONFIG_MONGODB_SERVER: db

ME_CONFIG_MONGODB_HOSTNAME: db

#API CONFIG

API_HOST=api

API_PORT=8000

#AUTH CONFIG

AUTH_HOST=https://fnucauth-1-t5925365.deta.app/